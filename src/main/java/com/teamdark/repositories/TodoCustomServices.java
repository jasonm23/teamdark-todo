package com.teamdark.repositories;

import org.springframework.beans.factory.annotation.Autowired;

public class TodoCustomServices implements TodoServices {
    @Autowired
    TodoRepository todoRepository;

    public void toggleCompleted(String id) {
        todoRepository.findOne(id).toggleCompleted();
    }
}
