package com.teamdark.repositories;

import com.teamdark.domain.Todo;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface TodoRepository extends JpaRepository<Todo, String> {
    public List<Todo> findAllByOrderByIdAsc();
    public List<Todo> findAllByOrderByTaskAsc();
    public Todo findById(Long id);
}
