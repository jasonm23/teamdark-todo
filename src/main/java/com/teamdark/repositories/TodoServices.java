package com.teamdark.repositories;

public interface TodoServices {
    public void toggleCompleted(String id);
}
