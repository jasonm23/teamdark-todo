package com.teamdark.repositories;

import com.teamdark.domain.AppUser;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface UserRepository extends JpaRepository<AppUser, String> {
    public AppUser findByName(String name);
    public AppUser findById(Long id);
    public List<AppUser> findAllByOrderByNameAsc();
    public List<AppUser> findAllByOrderByIdAsc();
}
