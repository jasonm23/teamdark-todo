package com.teamdark.domain;

import com.teamdark.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.*;
import java.util.UUID;

@Entity
public final class Todo {

    @GeneratedValue
    @Id Long id;

    private volatile String task;

    @ManyToOne
    @JoinColumn(name ="app_user_id")
    private volatile AppUser appUser;

    public volatile boolean completed = false;

    public Todo(){ }

    public Todo(String task) {
        this.task = task;
    }

    public Todo(String task, AppUser appUser) {
        this.task = task;
        this.appUser = appUser;
    }

    public Long getId() {
        return id;
    }

    public void setTask(String task) {
        this.task = task;
    }

    public String getTask() {
        return task;
    }

    public void setAppUser(AppUser appUser) {
        this.appUser = appUser;
    }

    public AppUser getAppUser() {
        return appUser;
    }

    public boolean toggleCompleted() {
        return completed = !completed;
    }

    public boolean getCompleted() {
      return completed;
    }

    public void setCompleted(boolean value){
        completed = value;
    }

}
