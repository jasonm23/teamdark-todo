package com.teamdark.domain;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

@Entity
@Table
public final class AppUser {

    @GeneratedValue
    @Id Long id;
    public volatile String name;
    public volatile String email;
    public volatile String password;

    @OneToMany(mappedBy = "appUser")
    @OrderBy("id ASC")
    public Set<Todo> todos = new HashSet<Todo>();

    public AppUser() {
    }

    public AppUser(String name) {
        this.name = name;
    }

    public AppUser(String name, String password) {
        this.name = name;
        this.password = password;
    }

    public AppUser(String name, String email, String password) {
        this.name = name;
        this.email = email;
        this.password = password;
    }

    public Long getId() {
        return id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmail() {
        return email;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPassword() {
        return password;
    }
}
