package com.teamdark.dao;

import com.teamdark.domain.AppUser;

/**
 * Created by neo on 4/3/16.
 */
public interface UserDao {
    AppUser createNewUserWithName(String username);
    AppUser createNewUserWithNameEmailPassword(String username, String email, String password);
}
