package com.teamdark.dao;

import com.teamdark.domain.AppUser;
import org.springframework.context.annotation.Configuration;

@Configuration
public class UserDaoImpl implements UserDao {
    public UserDaoImpl() {
    }

    @Override
    public AppUser createNewUserWithName(String username){
        return new AppUser(username);
    }

    @Override
    public AppUser createNewUserWithNameEmailPassword(String username, String email, String password){
        return new AppUser(username, email, password);
    }
}
