package com.teamdark.web;

import com.teamdark.domain.AppUser;
import com.teamdark.domain.Todo;
import com.teamdark.repositories.TodoRepository;
import com.teamdark.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
public class TodoController {

    private final TodoRepository todoRepository;
    private final UserRepository userRepository;

    @Autowired
    TodoController(TodoRepository todoRepository, UserRepository userRepository) {
        this.todoRepository = todoRepository;
        this.userRepository = userRepository;
    }

    @RequestMapping("/")
    public String index(ModelMap model) {
        List<Todo> todoList = todoRepository.findAllByOrderByIdAsc();
        List<AppUser> userList = userRepository.findAllByOrderByNameAsc();
        model.addAttribute("allUsers", userList);
        model.addAttribute("todoList", todoList);
        model.addAttribute("todo", new Todo());
        return "todo";
    }

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public String add(Todo todo,
                      @RequestParam("appUserId") Long appUserId,
                      @RequestParam("redirect") String redirect) {
        todo.setAppUser(userRepository.findById(appUserId));
        todoRepository.saveAndFlush(todo);
        return redirect(redirect);
    }

    @RequestMapping(value = "/toggle/{id}")
    public String complete(@PathVariable Long id, @RequestHeader(value = "referer", required = false) final String referer) {
        Todo todo = this.todoRepository.findById(id);
        todo.toggleCompleted();
        todoRepository.saveAndFlush(todo);
        return redirect(referer);
    }

    @RequestMapping(value = "/delete/{id}")
    public String delete(@PathVariable Long id, @RequestHeader(value = "referer", required = false) final String referer) {
        Todo todo = todoRepository.findById(id);
        todoRepository.delete(todo);
        return redirect(referer);
    }

    private String redirect(String location) {
        return "redirect:" + ((location != null) ? location : "/");
    }

}
