package com.teamdark.web;


import com.teamdark.dao.UserDaoImpl;
import com.teamdark.domain.AppUser;
import com.teamdark.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class SessionController {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserDaoImpl userDaoImpl;

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String login() {
        return "login";
    }

    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public String doLogin(@RequestParam("username") String username) {
        AppUser appUser = userRepository.findByName(username);
        if (appUser == null) {
            appUser = userDaoImpl.createNewUserWithName(username);
            userRepository.saveAndFlush(appUser);
        }
        return "redirect:/user/" + appUser.getId();
    }
}
