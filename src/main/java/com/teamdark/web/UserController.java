package com.teamdark.web;

import com.teamdark.domain.AppUser;
import com.teamdark.domain.Todo;
import com.teamdark.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.validation.Valid;
import java.util.List;

@Controller
public class UserController {

    private final UserRepository userRepository;

    @Autowired
    UserController(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @RequestMapping("/users")
    public String index(ModelMap model) {
        List<AppUser> userList = userRepository.findAllByOrderByNameAsc();
        model.addAttribute("userList", userList);
        return "users";
    }

    @RequestMapping("/user/{id}")
    public String show(@PathVariable Long id, Model model) {
        AppUser appUser = userRepository.findById(id);
        model.addAttribute("todoList", appUser.todos);
        model.addAttribute("todo", new Todo());
        model.addAttribute("appUser", appUser);
        return "user";
    }

    @RequestMapping(value = "/users/add", method = RequestMethod.GET)
    public String addForm(AppUser appUser) {
        return "add_user";
    }

    @RequestMapping(value = "/users/add", method = RequestMethod.POST)
    public String add(@Valid AppUser appUser, BindingResult bindingResult, Model model) {
        userRepository.saveAndFlush(appUser);
        model.addAttribute("name", appUser.getName());
        model.addAttribute("password", appUser.getPassword());
        if(bindingResult.hasErrors()){
            return "add_user";
        }
        return "redirect:/users";
    }

    @RequestMapping(value = "/user/{id}/delete")
    public String delete(@PathVariable Long id) {
        AppUser appUser = userRepository.findById(id);
        userRepository.delete(appUser);
        return "redirect:/users";
    }
}
