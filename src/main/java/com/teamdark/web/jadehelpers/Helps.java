package com.teamdark.web.jadehelpers;

import com.domingosuarez.boot.autoconfigure.jade4j.JadeHelper;

@JadeHelper
public class Helps {
    public String css(String href) {
        return String.format("<link rel='stylesheet' type='text/css' href='/css/%s.css'/>", href);
    }
}
