package com.teamdark.domain;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class AppUserTest {

    @Before
    public void setUp() throws Exception {

    }

    @After
    public void tearDown() throws Exception {

    }
    @Test
    public void testExtendedUserConstructor() throws Exception {
        AppUser user = new AppUser("name", "email", "password");
        assertEquals(user.getName(), "name");
        assertEquals(user.getEmail(), "email");
        assertEquals(user.getPassword(), "password");
    }

    @Test
    public void testUserConstructor() throws Exception {
        AppUser appUser = new AppUser("name", "password");
        assertEquals(appUser.getName(), "name");
        assertEquals(appUser.getPassword(), "password");
    }

    @Test
    public void testSimpleUserConstructor() throws Exception {
        AppUser appUser = new AppUser("name");
        assertEquals(appUser.getName(), "name");
        assertNull(appUser.getEmail());
    }
}
