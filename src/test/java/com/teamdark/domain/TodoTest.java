package com.teamdark.domain;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Created by neo on 4/3/16.
 */
public class TodoTest {

    private Todo todo;

    @Before
    public void setUp() throws Exception {
        //create a test variable with complete status false
        todo = new Todo();
        todo.setTask("new task");
    }

    @After
    public void tearDown() throws Exception {

    }

    @Test
    public void testToggleCompleted() throws Exception {
        boolean initialStatus = todo.getCompleted();
        todo.toggleCompleted();
        boolean toggledOnceStatus = todo.getCompleted();
        todo.toggleCompleted();
        boolean toggledTwiceStatus = todo.getCompleted();

        assertFalse(initialStatus);
        assertTrue(toggledOnceStatus);
        assertFalse(toggledTwiceStatus);

    }
}
