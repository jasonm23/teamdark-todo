package com.teamdark.web;

import com.teamdark.TodoApplication;
import com.teamdark.dao.UserDaoImpl;
import com.teamdark.domain.AppUser;
import com.teamdark.repositories.UserRepository;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = TodoApplication.class)
@WebAppConfiguration
public class SessionControllerTest {

//    private MockMvc mockMvc;
//
//    //@Mock
//    @Autowired
//    private UserRepository userRepository;

    @Autowired
    String helloStringBean;
//
//    @InjectMocks
//    public SessionController controller;
//
//    @Mock
//    private UserDaoImpl userDaoImpl;

    @Before
    public void setUp() throws Exception {
//        InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
//        viewResolver.setPrefix("/WEB-INF/templates");
//        viewResolver.setSuffix(".html");
//
//        MockitoAnnotations.initMocks(this);
//        this.mockMvc = MockMvcBuilders.standaloneSetup(controller)
//                .setViewResolvers(viewResolver)
//                .build();

        System.out.println(helloStringBean);
    }

    @After
    public void tearDown() throws Exception {

    }

    @Test
    public void fakeTest() throws Exception {

    }

//    @Test
//    public void testLogin() throws Exception {
//        mockMvc.perform(get("/login"))
//                .andExpect(status().isOk())
//                .andExpect(view().name("login"));
//    }
//
//    @Test
//    public void testDoLoginForExistingUser() throws Exception {
//
//        String existingName = "exist name";
//
//        AppUser existingUser = new AppUser(existingName, "exist email", "exist password");
//        String expectUrl = "redirect:/user/" + existingUser.getId();
//
//        when(userRepository.findByName(any(String.class))).thenReturn(existingUser);
//
//        mockMvc.perform(post("/login")
//                .param("username", existingName)
//        )
//                .andExpect(status().is3xxRedirection())
//                .andExpect(view().name(expectUrl));
//
//        verify(userRepository, times(1)).findByName(existingName);
//    }
//
//
//    @Test
//    public void testDoLoginForNonExistentUser() throws Exception {
//        String nonExistentName = "non exist name";
//        AppUser newUser = new AppUser(nonExistentName, "email", "password");
//
//        when(userDaoImpl.createNewUserWithName(any(String.class))).thenReturn(newUser);
//
//        mockMvc.perform(post("/login")
//                .param("username", nonExistentName)
//        )
//                .andExpect(status().is3xxRedirection())
//                .andExpect(view().name("redirect:/user/" + newUser.getId()));
//
//        verify(userRepository, times(1)).saveAndFlush(newUser);
//    }

}
