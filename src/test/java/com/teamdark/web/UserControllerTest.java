package com.teamdark.web;

import com.teamdark.domain.AppUser;
import com.teamdark.domain.Todo;
import com.teamdark.repositories.TodoRepository;
import com.teamdark.repositories.UserRepository;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@ActiveProfiles(profiles = "test")
@Transactional
public class UserControllerTest {

    private MockMvc mockMvc;

    @Autowired
    private TodoRepository todoRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserController controller;

    @Before
    public void setUp() throws Exception {
        InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
        viewResolver.setPrefix("/WEB-INF/templates");
        viewResolver.setSuffix(".html");

        MockitoAnnotations.initMocks(this);
        this.mockMvc = MockMvcBuilders.standaloneSetup(controller)
            .setViewResolvers(viewResolver)
            .build();
    }

    @After
    public void tearDown() throws Exception {
        todoRepository.deleteAll();
        userRepository.deleteAll();
    }

    @Test
    public void testShow() throws Exception {
        // setup user with todos
        AppUser appUser = new AppUser("new user", "password123");
        Todo first_todo = new Todo("first task");
        Todo second_todo = new Todo("second task");

        appUser.todos.add(first_todo);
        appUser.todos.add(second_todo);

        // mock up user repository
        when(userRepository.findById(appUser.getId())).thenReturn(appUser);

        String getUrl = "/user/" + appUser.getId().toString();

        // do post
        mockMvc.perform(get(getUrl))
            .andExpect(status().isOk())
            .andExpect(view().name("user"))
            .andExpect(model().attribute("user", hasProperty("name", is("new user"))))
            .andExpect(model().attribute("user", hasProperty("password", is("password123"))));
    }

    @Test
    public void testAddForm() throws Exception {
        mockMvc.perform(get("/users/add"))
            .andExpect(status().isOk())
            .andExpect(view().name("add_user"));
    }

    @Test
    public void testAdd() throws Exception {
        mockMvc.perform(post("/users/add")
                        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                        .param("name", "new user")
                        .param("password", "password123")
                        .sessionAttr("appUser", new AppUser()))
                .andExpect(status().is3xxRedirection())
                .andExpect(view().name("redirect:/users"));

    }
}
