package com.teamdark.web;

import com.teamdark.TodoApplication;
import com.teamdark.domain.AppUser;
import com.teamdark.domain.Todo;
import com.teamdark.repositories.TodoRepository;
import com.teamdark.repositories.UserRepository;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.test.context.web.ServletTestExecutionListener;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;

import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = TodoApplication.class)
@WebAppConfiguration
@TestExecutionListeners(
                        listeners={ServletTestExecutionListener.class,
                                   DependencyInjectionTestExecutionListener.class,
                                   DirtiesContextTestExecutionListener.class,
                                   TransactionalTestExecutionListener.class}
                        )
@ActiveProfiles(profiles = "test")
@Transactional
public class TodoControllerTest {

    @Autowired
    private WebApplicationContext wac;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private TodoRepository todoRepository;

    private MockMvc mockMvc;
    private Todo todo;
    private AppUser appUser;

    @Before
    public void setUp() throws Exception {
        mockMvc = webAppContextSetup(wac).build();
    }

    @After
    public void tearDown() throws Exception {

    }

    @Test
    public void testIndex() throws Exception {
        Todo todo = createTodoWithTestUser();
        mockMvc.perform(get("/"))
                .andExpect(status().isOk())
                .andExpect(view().name("todo"))
                .andExpect(model().attribute("todoList", hasItem(todo)));
    }

    @Test
    public void testAdd() throws Exception {

    }

    @Test
    public void testComplete() throws Exception {

    }

    @Test
    public void testDelete() throws Exception {

    }

    private Todo createTodoWithTestUser() {
        todo = new Todo("some task");
        todo = todoRepository.save(todo);

        appUser = new AppUser("test_user", "password");
        appUser = userRepository.save(appUser);
        appUser.todos.add(todo);
        userRepository.save(appUser);
        return todo;
    }
}
