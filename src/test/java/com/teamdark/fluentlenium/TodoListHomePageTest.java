package com.teamdark.fluentlenium;

import com.teamdark.domain.AppUser;
import org.junit.Before;
import org.junit.Test;
import org.springframework.transaction.annotation.Transactional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

public class TodoListHomePageTest extends TodoBaseFluentTest {

    String taskName = "Do something amazing!";
    String testUserName = "Test User";
    String title = "Team Dark - Spring Todo";

    @Before
    public void setup() {
        AppUser appUser = addTestUser(testUserName);
        goLocal("/");
        fillSelect("select[name=appUserId]").withText(testUserName);
        fill("input[name=task]").with(taskName);
        submit("#add-task");
    }

    @Test
    public void title_contains_spring_todo() {
        assertThat(find("h1").getText()).contains(title);
    }

    @Test
    public void lists_tasks() {
        // Check table header
        assertThat(find("th").getText()).contains("Task");
    }

    @Test
    public void tasks_show_owner() {
        assertThat(find("td.owner-column").getText()).contains(testUserName);
    }

    @Test
    public void complete_task() {
        findFirst("a.complete-toggle").click();
        String completedTask = findFirst("span.completed").getText();
        assertThat(completedTask.equals(taskName));
        findFirst("a.complete-toggle").click();
    }

    @Test
    public void delete_task_via_todo_list() throws InterruptedException {
        find("td.action-column").find("a.delete-button").click();
        assertThat(find("table").isEmpty());
    }

    @Test
    public void add_task_via_todo_list() throws InterruptedException {
        // @Before added our test task
        assertThat(find(".task").getText().equals(taskName));
    }

}
