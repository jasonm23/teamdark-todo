package com.teamdark.fluentlenium;

import com.teamdark.domain.AppUser;
import org.junit.Before;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.fluentlenium.core.filter.FilterConstructor.withText;

public class UserPageTest extends TodoBaseFluentTest {

    String taskName = "Do something amazing!";
    String testUserName = "Test User";
    String title = "Team Dark - Spring Todo";

    @Before
    public void setup() {
        AppUser appUser = addTestUser(testUserName);
        goLocal("/user/" + appUser.getId().toString());
    }

    @Test
    public void page_heading() {
        assertThat(find("h2").getText().contains("for " + testUserName));
    }

    @Test
    public void add_task_for_user() {
        add_task_via_form();
        assertThat(find("h2").getText().contains("Todo Tasks for " + testUserName));
        assertThat(find("td.task").getText().equals(taskName));
    }

    @Test
    public void complete_task_for_user() {
        add_task_via_form();
        assertThat(find("a.complete-toggle").click());
        assertThat(find("td.task").find(".completed").getText().equals(taskName));
    }

    @Test
    public void delete_task_for_user() {
        add_task_via_form();
        assertThat(find("h2").getText().contains("Todo Tasks for " + testUserName));
        assertThat(find("a.delete-button").click());
        assertThat(find("h2").getText().contains("No Tasks for " + testUserName));
        assertThat(find("table").isEmpty());
    }

    private void add_task_via_form() {
        fill("input#task").with(taskName);
        submit("#user-task-add");
    }

}
