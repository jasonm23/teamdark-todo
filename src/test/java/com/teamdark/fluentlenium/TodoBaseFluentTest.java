package com.teamdark.fluentlenium;

import com.teamdark.TodoApplication;
import com.teamdark.domain.AppUser;
import com.teamdark.domain.Todo;
import com.teamdark.repositories.TodoRepository;
import com.teamdark.repositories.UserRepository;
import org.fluentlenium.adapter.FluentTest;
import org.junit.After;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.context.annotation.Profile;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

@ActiveProfiles(profiles = "test")
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = TodoApplication.class)
@WebAppConfiguration
@IntegrationTest("server.port:0")
public class TodoBaseFluentTest extends FluentTest {

    @Autowired
    TodoRepository todoRepository;

    @Autowired
    UserRepository userRepository;

    @Value("${local.server.port}")
    private int port;

    protected void goLocal(String url) {
        goTo("http://localhost:" + port + url);
    }

    @After
    public void truncateTables() {
        // When we access repositories, we clear them
        // after running each spec
        todoRepository.deleteAll();
        userRepository.deleteAll();
    }

    protected Todo addTestTodo(String task) {
        Todo todo = new Todo(task);
        todoRepository.saveAndFlush(todo);
        return todo;
    }

    protected Todo addTestTodo(String task, AppUser appUser) {
        Todo todo = new Todo(task, appUser);
        todoRepository.saveAndFlush(todo);
        return todo;
    }

    protected AppUser addTestUser(String name) {
        AppUser appUser = new AppUser(name);
        userRepository.saveAndFlush(appUser);
        return appUser;
    }
}
