package com.teamdark.fluentlenium;

import com.teamdark.domain.AppUser;
import org.junit.Before;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.fluentlenium.core.filter.FilterConstructor.withText;

public class UsersPageTest extends TodoBaseFluentTest {

    String taskName = "Do something amazing!";
    String testUserName = "Test User";
    String title = "Team Dark - Spring Todo";

    @Before
    public void setup() {
        AppUser appUser = addTestUser(testUserName);
        goLocal("/users/");
    }

    @Test
    public void list_users() {
        assertThat(find("td.user-name").getText().equals(testUserName));
    }

    @Test
    public void add_user() {
        assertThat(find("a.add-user-button").click());
        assertThat(find("h3").getText().equals("Add New User"));
        fill("#name").with("Rasmus Lerdorf");
        fill("#password").with("password");
        submit("#add-new-user-form");
        assertThat(find("td.user-name", withText("Rasmus Lerdorf")));
    }

    @Test
    public void delete_user() {
        assertThat(find("a.delete-button").click());
        assertThat(find("table").isEmpty());
    }
}
